﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.FtpClient;
using System.Threading.Tasks;
using log4net;
using SS.FTPDownloader.Objects;
using SS.FTPDownloader.Objects.Extensions;
using SS.FTPDownloader.Utilities;

// ReSharper disable EventNeverSubscribedTo.Global
// ReSharper disable UnusedMemberInSuper.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global

namespace SS.FTPDownloader.FTP
{
	public interface IFtpWrapper : IDisposable
	{
		event EventHandler<DownloadedEventArgs> OnDownloadComplete;
		event EventHandler<ProgressEventArgs> OnProgressUpdated;
		event EventHandler<DownloadedEventArgs> OnDownloadStarted;
		event EventHandler<DownloadedEventArgs> OnDownloadError;
		
		void AppendToDownloadList(List<FtpListItem> ftpListItems);

		void Connect();

		void DeleteFile(FtpListItem file);

		void Disconnect();

		bool DownloadFile(FtpListItem file);

		void DownloadFiles(int maxPerSession = int.MaxValue);

		IEnumerable<FtpListItem> GetAllFiles(string dir);

		Task<IEnumerable<FtpListItem>> GetAllFilesAsync(string dir);

		FtpListItem GetFile(string location);
	}

	public class FtpWrapper : IFtpWrapper
	{
		private static bool _downloadStatus;

		public static DistinctObservableConcurrentQueue<FtpListItem> DownloadList;

		public static FtpListItem CurrentItem
		{
			get { return _currentItem; }
			private set
			{
				Log.Debug($"CurrentItem is now {value.FullName}");
				_currentItem = value;
			}
		}

		private readonly IFtpClient _client;
		private readonly Settings _settings;

		private readonly Dictionary<string, int> _currentErrors;

		private static readonly ILog Log = LogManager.GetLogger(typeof(FtpWrapper));
		private static FtpListItem _currentItem;

		public FtpWrapper(Settings settings) : 
			this(FtpClient(settings), settings)
		{
		}

		internal FtpWrapper(IFtpClient ftpClient, Settings settings)
		{
			_currentErrors = new Dictionary<string, int>();
			if (DownloadList == null)
			{
				DownloadList = new DistinctObservableConcurrentQueue<FtpListItem>();
			}
			_client = ftpClient;
			_settings = settings;
		}

		public event EventHandler<DownloadedEventArgs> OnDownloadComplete;
		public event EventHandler<DownloadedEventArgs> OnDownloadStarted;
		public event EventHandler<DownloadedEventArgs> OnDownloadError;
		public event EventHandler<ProgressEventArgs> OnProgressUpdated;
		

		public void AppendToDownloadList(List<FtpListItem> ftpListItems)
		{
			if (DownloadList == null)
			{
				DownloadList = new DistinctObservableConcurrentQueue<FtpListItem>();
			}

			if (ftpListItems != null)
			{
				DownloadList.AddRange(ftpListItems, new FtpListItemComparer());
			}
		}

		public void Connect()
		{
			_client.Connect();
			_client.SetWorkingDirectory(_settings.FtpRootCheckDir);
			_client.EnableThreadSafeDataConnections = true;
		}

		public void DeleteFile(FtpListItem file)
		{
			_client.DeleteFile(file.FullName);

			if (file.FullName == "") return;

			var path = Path.GetDirectoryName(file.FullName);
			while (path != "\\" + _settings.FtpRootCheckDir && DeleteFolder(path))
			{
				path = Path.GetDirectoryName(path);
			}
		}

		public void Disconnect()
		{
			_client.Disconnect();
		}

		public bool DownloadFile(FtpListItem file)
		{
			var filePath = _settings.TempDownloadPath + file.Name;

			try
			{
				var fileExists = File.Exists(filePath);
				
				var stats = new ProgressStatistic
				{
					CurrentBytesSampleCount = 50
				};

				stats.ProgressChanged += OnProgressUpdated;
				stats.Finished += OnProgressUpdated;

				if (!_client.FileExists(file.FullName))
				{
					Log.Warn($"Attempted to download {file.Name}, but it no longer exists on the server.");
					DownloadError(new DownloadedEventArgs(new DownloadedFile {DownloadLocation = filePath, FtpListItem = file}));
					return false;
				}

				DownloadStarted(new DownloadedEventArgs(new DownloadedFile { DownloadLocation = filePath, FtpListItem = file }, stats));

				using (var local = new FileStream(filePath, fileExists ? FileMode.Append : FileMode.Create))
				{					
					using (var stream = _client.OpenRead(file.FullName, local.Position))
					{
						var args = new CopyFromArguments(stats.ProgressChange, new TimeSpan(0, 0, 1), stream.Length);
						Log.Info($"{(fileExists ? "Resuming" : "Downloading")} {file.Name}");
						local.CopyFrom(stream, args);
					}
				}

				Log.Info("Completed " + file.Name);

				DownloadComplete(new DownloadedEventArgs(new DownloadedFile {DownloadLocation = filePath, FtpListItem = file}, stats));

				return true;
			}

			catch (FtpException e)
			{
				DownloadError(new DownloadedEventArgs(new DownloadedFile { DownloadLocation = filePath, FtpListItem = file }));
				Log.Error("FTP error with " + file.Name);
				Log.Error(e.Message);
				TrackErrorCount(file);
				return false;
			}
			
			catch (NotSupportedException)
			{
				//The file exists, but doesn't support seeking to the end for resume.
				//Delete the existing file and try again from scratch.
				File.Delete(filePath);
				return DownloadFile(file);
			}

			catch (Exception e)
			{
				DownloadError(new DownloadedEventArgs(new DownloadedFile { DownloadLocation = filePath, FtpListItem = file }));
				Log.Error("Error with " + file.Name);
				Log.Error(e.Message);
				TrackErrorCount(file);
				return false;
			}
		}

		public void DownloadFiles(int maxPerSession = 0)
		{
			if (_downloadStatus)
			{
				return;
			}

			_downloadStatus = true;

			var downloadCount = 0;
			
			if (maxPerSession != 0)
			{
				Log.Info($"Beginning session of {maxPerSession} downloads.");
			}

			FtpListItem item;

			while ((maxPerSession == 0 || downloadCount < maxPerSession) && DownloadList.TryDequeue(out item))
			{
				CurrentItem = item;

				try
				{
					if (DownloadFile(item))
					{
						downloadCount++;
						DeleteFile(item);
					}
				}
				catch (Exception e)
				{
					Log.Error($"{e.Message}");
					TrackErrorCount(item);
				}
			}

			_downloadStatus = false;
		}

		public IEnumerable<FtpListItem> GetAllFiles(string dir)
		{
			return GetAllFilesAsync(dir).Result;
		}

		public async Task<IEnumerable<FtpListItem>> GetAllFilesAsync(string dir)
		{
			var root = _client.GetListing(dir);

			var res = new List<FtpListItem>();

			res.AddRange(root.Where(x => x.Type == FtpFileSystemObjectType.File));

			var taskList =
				root.Where(x => x.Type == FtpFileSystemObjectType.Directory)
					.Select(x => GetAllFilesAsync(x.FullName))
					.ToList();

			foreach (var ftpDirItem in await Task.WhenAll(taskList))
			{
				res.AddRange(ftpDirItem);
			}

			return res.OrderBy(x => x.FullName);
		}

		public FtpListItem GetFile(string location)
		{
			return _client.GetListing(location).SingleOrDefault();
		}

		public bool DeleteFolder(string folderPath)
		{
			try
			{
				_client.DeleteDirectory(folderPath);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public static IFtpClient FtpClient(Settings settings)
		{
			return new FtpClient
			{
				Credentials = new NetworkCredential(settings.FtpUserName, settings.FtpPassword),
				Host = settings.FtpUrl,
				DataConnectionEncryption = true,
				Port = 21
			};
		}

		private void DownloadComplete(DownloadedEventArgs e)
		{
			OnDownloadComplete?.Invoke(this, e);
		}

		private void DownloadStarted(DownloadedEventArgs e)
		{
			OnDownloadStarted?.Invoke(this, e);
		}

		private void DownloadError(DownloadedEventArgs e)
		{
			OnDownloadError?.Invoke(this, e);
		}
		
		private void TrackErrorCount(FtpListItem file)
		{
			if (_currentErrors.ContainsKey(file.Name))
			{
				_currentErrors[file.Name]++;
			}
			else
			{
				_currentErrors.Add(file.Name, 1);
			}

			if (_currentErrors[file.Name] < _settings.MaxRetryCount)
			{
				DownloadList.Enqueue(file);
				return;
			}

			_currentErrors.Remove(file.Name);
			Log.Warn($"Removed {file.Name} from download list");
		}

		public void Dispose()
		{
			_client.Disconnect();
		}
	}
}