﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.FtpClient;
using System.Threading;
using SS.FTPDownloader.FTP;
using SS.FTPDownloader.Objects;
using SS.FTPDownloader.Utilities;

namespace SS.FtpDownloader
{
    public class AutoDownload
    {
        private readonly Action<string, bool> _logger;

        private readonly IFtpWrapper _ftpWrapper;

        private bool _isDownloading;

        private readonly object _downloadLock = new object();

        public AutoDownload() : this(null) { }

        public AutoDownload(Action<string, bool> logger)
            : this(logger, null)
        {
            _logger = logger;
        }

        public AutoDownload(Action<string, bool> logger, IFileUtilities utilities)
            : this(new FtpWrapper(s => logger(s, true)), new FileCleanup(s => logger(s, true), utilities))
        {
            _logger = logger;
        }

        public AutoDownload(IFtpWrapper ftpWrapper, IFileCleanup cleanup)
        {
            _ftpWrapper = ftpWrapper;
            _ftpWrapper.OnDownloadComplete += cleanup.CleanupFile;
            _isDownloading = false;
        }

        public void CheckServer(object errorCount)
        {
            var tryCount = errorCount as int? ?? 1;

            if (tryCount > 3)
            {
                _logger("An FTP connection error has occured.", true);
                return;
            }

            try
            {
                _ftpWrapper.Connect();
                var allFiles = getAllFilesOnServer();
                _ftpWrapper.Disconnect();

                if (allFiles.Any())
                {
                    _logger($"{allFiles.Count} files found. Determining eligability.", true);
                    ThreadPool.QueueUserWorkItem(x => processFoundFiles(allFiles));
                }
            }
            catch (FtpException)
            {
                CheckServer(++tryCount);
            }
            catch (Exception e)
            {
                _logger("An unknown error occured checking the server:", true);
                _logger(e.Message, false);
            }
        }

        private List<FtpListItem> getAllFilesOnServer()
        {
            return _ftpWrapper.GetAllFiles("").ToList();
        }

        private void processFoundFiles(IEnumerable<FtpListItem> originList)
        {
            Thread.Sleep(new TimeSpan(0, 0, 5));

            _ftpWrapper.Connect();
            var checkList = getAllFilesOnServer();
            _ftpWrapper.Disconnect();

            var toDownload = 0;

            foreach (var file in originList)
            {
                var check = checkList.SingleOrDefault(x => x.FullName == file.FullName);

                if (check == null || check.Modified != file.Modified || FtpWrapper.DownloadList.Contains(check, new FtpListItemComparer()))
                    continue;

                FtpWrapper.DownloadList.Add(check);
                toDownload++;
            }

            if (toDownload > 0)
            {
                _logger($"Found {toDownload} new files eligable for download.", true);
                ThreadPool.QueueUserWorkItem(x => downloadFiles());
            }
        }

        private void downloadFiles()
        {
            if (_isDownloading) return;

            lock (_downloadLock)
            {
                _isDownloading = true;
                _ftpWrapper.Connect();
                _ftpWrapper.DownloadFiles();
                _ftpWrapper.Disconnect();
                _isDownloading = false;
            }
        }
    }
}
