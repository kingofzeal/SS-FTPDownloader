﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.FtpClient;
using System.Threading;
using log4net;
using SS.FTPDownloader.FTP;
using SS.FTPDownloader.Objects;
using SS.FTPDownloader.Utilities;

namespace SS.FTPDownloader
{
	public class FtpEvents
	{
		public EventHandler<ProgressEventArgs> UpdatedProgressHandler { get; set; }
		public EventHandler<DownloadedEventArgs> DownloadStartedHandler { get; set; }
		public EventHandler<DownloadedEventArgs> DownloadErrorHandler { get; set; }
		public EventHandler<DownloadedEventArgs> DownloadCompletedHandler { get; set; }
		public EventHandler<FileCleanupEventArgs> CleanupStartedHandler { get; set; }
		public EventHandler<FileCleanupEventArgs> CleanupCompletedHandler { get; set; }
	}

	public class FtpDownloader
	{
		private readonly Settings _settings;
		private readonly BackgroundWorker _checkWorker = new BackgroundWorker();
		private readonly IFileCleanup _cleanup;
		private readonly IFtpWrapper _ftpWrapper;
		private readonly BackgroundWorker _processWorker = new BackgroundWorker();

		private List<DownloadedEventArgs> _downloadedFiles;
		private bool _isProcessing; 

		// ReSharper disable once NotAccessedField.Local
		private Timer _timer;
		private readonly FtpEvents _events;
		
		private static readonly ILog Log = LogManager.GetLogger(typeof(FtpDownloader));

		public FtpDownloader(Settings settings, FtpEvents events = null)
		{
			_events = events ?? new FtpEvents();
			_settings = settings;
			TvRenamer.SetRemapFile(settings.RemapFile);
			_ftpWrapper = GenerateFtpWrapper(_settings, _events);
			_cleanup = GenerateFileCleanup(_settings, _events);
		}

		public void BeginProcess()
		{
			_processWorker.WorkerReportsProgress = true;
			_processWorker.DoWork += DownloadFiles;

			_checkWorker.WorkerReportsProgress = true;
			_checkWorker.ProgressChanged += BeginDownloading;
			_checkWorker.DoWork += CheckServer;
			
			Log.Info($"FTP AutoDownloader v{typeof(FtpDownloader).Assembly.GetName().Version}\n");

			_timer = new Timer(BeginCheck, 1, new TimeSpan(0, 0, 5),
				new TimeSpan(0, _settings.NormalRecheckTime, 0));
		}
		
		private void BeginCheck(object o)
		{
			if (_checkWorker.IsBusy) return;

			_checkWorker.RunWorkerAsync(o);
		}

		private void BeginDownloading(object sender, ProgressChangedEventArgs args)
		{
			if (_processWorker.IsBusy) return;

			_processWorker.RunWorkerAsync();
		}

		private static FtpWrapper GenerateFtpWrapper(Settings settings, FtpEvents events)
		{
			var ftp = new FtpWrapper(settings);

			if (events == null)
			{
				return ftp;
			}

			ftp.OnProgressUpdated += events.UpdatedProgressHandler;
			ftp.OnDownloadStarted += events.DownloadStartedHandler;
			ftp.OnDownloadError += events.DownloadErrorHandler;
			ftp.OnDownloadComplete += events.DownloadCompletedHandler;
			
			return ftp;
		}

		private static FileCleanup GenerateFileCleanup(Settings settings, FtpEvents events)
		{
			var cleanup = new FileCleanup(settings.VideoRootPath);
			
			cleanup.CleanupStarted += events.CleanupStartedHandler;
			cleanup.CleanupFinished += events.CleanupCompletedHandler;

			return cleanup;
		}

		private void DownloadFiles(object sender, DoWorkEventArgs doWorkEventArgs)
		{
			while (FtpWrapper.DownloadList.Any())
			{
				if (_isProcessing)
				{
					return;
				}

				using (var ftp = GenerateFtpWrapper(_settings, _events))
				{
					if (_settings.BatchDownloads)
					{
						ftp.OnDownloadComplete += RecordBatchDownload;
					}
					else
					{
						ftp.OnDownloadComplete += _cleanup.CleanupFile;
					}

					ftp.Connect();
					ftp.DownloadFiles(_settings.BatchDownloads ? _settings.BatchDownloadSize : 0);
					ftp.Disconnect();
				}

				if (!_settings.BatchDownloads)
				{
					continue;
				}

				ProcessBatchDownloads();
			}
		}

		private void ProcessBatchDownloads()
		{
			if (_isProcessing) return;

			_isProcessing = true;

			foreach (var file in _downloadedFiles.ToList())
			{
				_cleanup.CleanupFile(this, file);
				_downloadedFiles.Remove(file);
			}

			_isProcessing = false;
		}

		private void RecordBatchDownload(object sender, DownloadedEventArgs downloadedEventArgs)
		{
			if (_downloadedFiles == null)
			{
				_downloadedFiles = new List<DownloadedEventArgs>();
			}

			_downloadedFiles.Add(downloadedEventArgs);
		}
		
		private void CheckServer(object sender, DoWorkEventArgs e)
		{
			var tryCount = e.Argument as int? ?? 1;

			if (tryCount > 3)
			{
				return;
			}

			try
			{
				_ftpWrapper.Connect();
				var allFiles = GetAllFilesOnServer();
				_ftpWrapper.Disconnect();

				if (!allFiles.Any()) return;

				Log.Info($"{allFiles.Count} files found. Determining eligability.");

				Thread.Sleep(new TimeSpan(0, 0, 5));

				_ftpWrapper.Connect();
				var checkList = GetAllFilesOnServer();
				_ftpWrapper.Disconnect();

				var toDownload = 0;

				foreach (var file in allFiles)
				{
					Log.Debug($"Found {file.FullName}.");
				}

				foreach (var file in allFiles)
				{
					var check = checkList.SingleOrDefault(x => x.FullName == file.FullName);

					if (check?.FullName == FtpWrapper.CurrentItem?.FullName || 
						check?.Modified != file.Modified ||
						FtpWrapper.DownloadList.Contains(check, new FtpListItemComparer()))
					{
						continue;
					}

					Log.Debug($"{file.FullName} added to queue.");
					FtpWrapper.DownloadList.Enqueue(check);
					toDownload++;
				}

				if (toDownload <= 0) return;

				Log.Info($"Found {toDownload} new files eligable for download.");
				_checkWorker.ReportProgress(toDownload);
			}
			catch (FtpException ex)
			{
				Log.Error("An FTP connection error has occured.");
				Log.Error(ex.Message);

				CheckServer(sender, new DoWorkEventArgs(++tryCount));
			}
			catch (Exception ex)
			{
				Log.Error("An unknown error occured checking the server:");
				Log.Error(ex.Message);
			}
		}
		
		private List<FtpListItem> GetAllFilesOnServer()
		{
			return _ftpWrapper.GetAllFiles("").ToList();
		}
	}
}
