﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.FtpClient;
using System.Threading;
using System.Windows.Forms;
using SS.FTPDownloader.FTP;
using SS.FTPDownloader.Objects;
using SS.FTPDownloader.Objects.Extensions;
using SS.FTPDownloader.Properties;
using SS.FTPDownloader.Utilities;
using Timer = System.Threading.Timer;

namespace SS.FTPDownloader
{
	public partial class AutoDownloadForm : Form
	{
		private const string DateTimeFormat = "MM/dd/yy HH:mm:ss";
		private const int ProgressBarResolution = 10000;

		private readonly BackgroundWorker _checkWorker = new BackgroundWorker();
		private readonly IFileCleanup _cleanup;
		private readonly IFtpWrapper _ftpWrapper;
		private readonly BackgroundWorker _processWorker = new BackgroundWorker();

		// ReSharper disable once NotAccessedField.Local
		private Timer _timer;

		public AutoDownloadForm()
		{
			InitializeComponent();

			_ftpWrapper = new FtpWrapper();
			_cleanup = new FileCleanup(Log);

			_processWorker.WorkerReportsProgress = true;
			_processWorker.DoWork += DownloadFiles;

			_checkWorker.WorkerReportsProgress = true;
			_checkWorker.ProgressChanged += BeginDownloading;
			_checkWorker.DoWork += CheckServer;

			FtpWrapper.DownloadList.OnAdd += FileAdded;
			FtpWrapper.DownloadList.OnRemove += RemoveFileFromList;

			Log($"FTP AutoDownloader v{typeof (Program).Assembly.GetName().Version}\n");

			_timer = new Timer(BeginCheck, 1, new TimeSpan(0, 0, 5),
				new TimeSpan(0, Settings.Default.NormalRecheckTime, 0));
		}

		private void BeginCheck(object o)
		{
			if (_checkWorker.IsBusy) return;

			_checkWorker.RunWorkerAsync(o);
		}

		private void BeginDownloading(object sender, ProgressChangedEventArgs args)
		{
			if (_processWorker.IsBusy) return;

			_processWorker.RunWorkerAsync();
		}

		private void DownloadFiles(object sender, DoWorkEventArgs doWorkEventArgs)
		{
			var ftp = new FtpWrapper(Log);
			ftp.OnProgressUpdated += UpdateProgressBar;
			ftp.OnDownloadComplete += _cleanup.CleanupFile;

			ftp.Connect();
			ftp.DownloadFiles();
			ftp.Disconnect();
		}

		private void FileAdded(object sender, ObservedListEventArgs observedListEventArgs)
		{
			var itemAdded = observedListEventArgs.Item as FtpListItem;

			if (itemAdded == null) return;

			downloadList.SafeInvoke(x => x.Items.Add(itemAdded.Name));
		}

		private void RemoveFileFromList(object sender, ObservedListEventArgs observedListEventArgs)
		{
			var itemRemoved = observedListEventArgs.Item as FtpListItem;

			if (itemRemoved == null) return;

			downloadList.SafeInvoke(x => x.Items.Remove(itemRemoved.Name));
		}

		private void CheckServer(object sender, DoWorkEventArgs e)
		{
			var tryCount = e.Argument as int? ?? 1;

			if (tryCount > 3)
			{
				Log("An FTP connection error has occured.");
				return;
			}

			try
			{
				_ftpWrapper.Connect();
				var allFiles = GetAllFilesOnServer();
				_ftpWrapper.Disconnect();

				if (!allFiles.Any()) return;

				Log($"{allFiles.Count} files found. Determining eligability.");
				Thread.Sleep(new TimeSpan(0, 0, 5));

				_ftpWrapper.Connect();
				var checkList = GetAllFilesOnServer();
				_ftpWrapper.Disconnect();

				var toDownload = 0;

				foreach (var file in allFiles)
				{
					var check = checkList.SingleOrDefault(x => x.FullName == file.FullName);

					if (check == null || check.Modified != file.Modified || FtpWrapper.DownloadList.Contains(check, new FtpListItemComparer()))
						continue;

					FtpWrapper.DownloadList.Add(check);
					toDownload++;
				}

				if (toDownload <= 0) return;

				Log($"Found {toDownload} new files eligable for download.");
				_checkWorker.ReportProgress(toDownload);
			}
			catch (FtpException ex)
			{
				if (tryCount == 3)
					Log(ex.Message);

				CheckServer(sender, new DoWorkEventArgs(++tryCount));
			}
			catch (Exception ex)
			{
				Log("An unknown error occured checking the server:");
				Log(ex.Message);
			}
		}

		private void UpdateProgressBar(object sender, ProgressEventArgs progressEventArgs)
		{
			var stat = progressEventArgs.ProgressStatistic;

			if (stat.HasFinished || double.IsNaN(stat.Progress))
			{
				progressBar.SafeInvoke(x => x.Value = 0);
				etaLabel.SafeInvoke(x => x.Text = "ETA");
				return;
			}

			progressBar.SafeInvoke(x => x.Maximum = ProgressBarResolution);
			progressBar.SafeInvoke(x => x.Value = (int) (stat.Progress * ProgressBarResolution));
			etaLabel.SafeInvoke(x => x.Text = $"ETA: {stat.EstimatedFinishingTime}");
		}

		private void Log(string message)
		{
			LogTextbox.SafeInvoke(x => x.AppendText(
				$"{DateTime.Now.ToString(DateTimeFormat)}: {message}{Environment.NewLine}"));
		}

		private List<FtpListItem> GetAllFilesOnServer()
		{
			return _ftpWrapper.GetAllFiles("").ToList();
		}
	}
}