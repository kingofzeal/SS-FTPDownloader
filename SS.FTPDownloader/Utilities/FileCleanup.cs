﻿using System;
using log4net;
using SS.FTPDownloader.Objects;
using SS.FTPDownloader.Objects.Media;

namespace SS.FTPDownloader.Utilities
{
	public interface IFileCleanup
	{
		#region public

		void CleanupFile(object sender, DownloadedEventArgs e);
		event EventHandler<FileCleanupEventArgs> CleanupStarted;
		event EventHandler<FileCleanupEventArgs> CleanupFinished;

		#endregion
	}

	public class FileCleanupEventArgs : EventArgs
	{
		public FileCleanupEventArgs(DownloadedFile file, ProgressStatistic stats = null)
		{
			File = file;
			ProgressStatistics = stats;
		}

		public DownloadedFile File { get; }

		public ProgressStatistic ProgressStatistics { get; set; }
	}

	public class FileCleanup : IFileCleanup
	{
		#region Fields
		
		private readonly IFileUtilities _utilities;

		private readonly IMediaFactory _mediaFactory;
		private readonly string _videoRootPath;

		#endregion

		#region Constructors
		
		public FileCleanup(string videoRootPath = "")
			: this(new FileUtilities(), videoRootPath)
		{ }

		public FileCleanup(IFileUtilities fileUtilities, string videoRootPath = "")
			: this(fileUtilities, new MediaFactory(), videoRootPath)
		{
		}

		internal FileCleanup(IFileUtilities fileUtilities, IMediaFactory mediaFactory, string videoRootPath = "")
		{
			_utilities = fileUtilities;
			_mediaFactory = mediaFactory;
			_videoRootPath = videoRootPath;
		}

		#endregion

		#region IFileCleanup Members

		public void CleanupFile(object sender, DownloadedEventArgs e)
		{
			var media = _mediaFactory.CreateMediaObject(e.File.DownloadLocation, _videoRootPath);

			cleanupBegin(new FileCleanupEventArgs(e.File, e.ProgressStatistics));
			media?.Process(_utilities);
			cleanupFinish(new FileCleanupEventArgs(e.File, e.ProgressStatistics));
		}

		public event EventHandler<FileCleanupEventArgs> CleanupStarted;
		public event EventHandler<FileCleanupEventArgs> CleanupFinished;

		#endregion


		private void cleanupBegin(FileCleanupEventArgs e)
		{
			CleanupStarted?.Invoke(this, e);
		}

		private void cleanupFinish(FileCleanupEventArgs e)
		{
			CleanupFinished?.Invoke(this, e);
		}
	}

}
