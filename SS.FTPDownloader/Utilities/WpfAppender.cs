﻿using log4net.Appender;
using System.ComponentModel;
using System.IO;
using System.Globalization;
using System.Runtime.CompilerServices;
using log4net.Core;
using SS.FTPDownloader.Annotations;

namespace SS.FTPDownloader.Utilities
{
	public class WpfAppender : AppenderSkeleton, INotifyPropertyChanged
	{
		private static string _notification;

		public string Notification
		{
			get { return _notification; }
			set {
				if (_notification == value)
					return;
				
				_notification = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		

		protected override void Append(LoggingEvent loggingEvent)
		{
			var writer = new StringWriter(CultureInfo.InvariantCulture);
			Layout.Format(writer, loggingEvent);
			Notification += writer.ToString();
		}
	}
}
