﻿
namespace SS.FTPDownloader.Utilities
{
	public class Settings
	{
		public int NormalRecheckTime { get; set; }
		public int ExpeditedRecheckTime { get; set; }
		public string FtpRootCheckDir { get; set; }
		public string TempDownloadPath { get; set; }
		public string RemapFile { get; set; }
		public int MaxRetryCount { get; set; }
		public int ProgressBarSize { get; set; }
		public string VideoRootPath { get; set; }
		public bool BatchDownloads { get; set; }
		public int BatchDownloadSize { get; set; }

		public string FtpUserName { get; set; }
		public string FtpPassword { get; set; }
		public string FtpUrl { get; set; }
	}
}
