﻿using System.IO;
using SS.FTPDownloader.Objects.Media;

namespace SS.FTPDownloader.Utilities
{
    public interface IFileUtilities
    {
		/// <summary>
		/// Creates all directories, if needed, for the given path
		/// </summary>
		/// <param name="path">The path to create directories for
		/// Can be a file path or directory path</param>
		void MakeDirs(string path);

		/// <summary>
		/// Moves the specified file to the destination
		/// If the file name already exists in the destination location, an iteration number is appended to the file
		/// </summary>
		/// <param name="file">The file to be moved</param>
		/// <param name="destination">The location to move the file to</param>
		void Move(string file, string destination);
    }

    public class FileUtilities : IFileUtilities
    {
		/// <summary>
		/// Creates all directories, if needed, for the given path
		/// </summary>
		/// <param name="path">The path to create directories for
		/// Can be a file path or directory path</param>
        public void MakeDirs(string path)
        {
            var directoryPath = Path.HasExtension(path) ? Path.GetDirectoryName(path) : path;

            if (directoryPath != null)
                Directory.CreateDirectory(directoryPath);
        }

		/// <summary>
		/// Moves the specified file to the destination
		/// If the file name already exists in the destination location, an iteration number is appended to the file
		/// </summary>
		/// <param name="file">The file to be moved</param>
		/// <param name="destination">The location to move the file to</param>
        public void Move(string file, string destination)
        {
            var dest = destination;
            var iteration = 1;

            while (!MoveFile(file, dest))
            {
                iteration++;

                var extension = Path.GetExtension(destination);
                var filePath = Path.GetDirectoryName(destination);
                var fileName = Path.GetFileNameWithoutExtension(destination);

                dest = $@"{filePath}\{fileName} ({iteration}){extension}";
            }
        }

		/// <summary>
		/// Attempts to move the specified file to the destination 
		/// </summary>
		/// <param name="file">The file to be moved</param>
		/// <param name="destination">The location to move the file to</param>
		/// <returns>True if successful, false if not successful</returns>
		private static bool MoveFile(string file, string destination)
        {
            try
            {
                File.Move(file, destination);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }

}
