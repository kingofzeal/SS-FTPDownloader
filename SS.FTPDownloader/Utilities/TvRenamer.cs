﻿using System.IO;
using System.Linq;
using SS.FTPDownloader.Objects;

namespace SS.FTPDownloader.Utilities
{
	public static class TvRenamer
	{
		private static string _remapFile;

		public static void SetRemapFile(string remapFile)
		{
			_remapFile = remapFile;
		}

		private static Remappings _remappings;

		private static void LoadRemappings()
		{
			if (_remappings == null)
				_remappings = new Remappings();
			

			if (string.IsNullOrWhiteSpace(_remapFile) || !File.Exists(_remapFile))
				return;
			
			var lastWriteTime = File.GetLastWriteTime(_remapFile);
			var file = File.ReadLines(_remapFile);

			if (lastWriteTime <= _remappings.Timestamp)
				return;

			var mapping = file
				.Select(line => line.Split(','))
				.ToDictionary(line => line[0].ToLower(), line => line[1]);

			_remappings.Mappings = mapping;
			_remappings.Timestamp = lastWriteTime;
		}

		public static string Rename(string originalName)
		{
			if (string.IsNullOrWhiteSpace(originalName))
				return "";

			LoadRemappings();
			var map = _remappings.Mappings;

			var nameToCheck = originalName.ToLower();
			
			return map.ContainsKey(nameToCheck)
				? map[nameToCheck]
				: originalName;
		}
	}
}
