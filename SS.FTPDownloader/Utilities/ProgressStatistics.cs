﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable EventNeverSubscribedTo.Global

namespace SS.FTPDownloader.Utilities
{
	public class ProgressEventArgs : EventArgs
	{
		public ProgressStatistic ProgressStatistic { get; private set; }

		public ProgressEventArgs(ProgressStatistic progressStatistic)
		{
			if (progressStatistic == null)
			{
				throw new ArgumentNullException(nameof(progressStatistic));
			}
			ProgressStatistic = progressStatistic;
		}
	}

	[Serializable]
	public class OperationAlreadyStartedException : Exception
	{
		public OperationAlreadyStartedException()
		{
		}

		public OperationAlreadyStartedException(string message) : base(message)
		{
		}

		public OperationAlreadyStartedException(string message, Exception inner) : base(message, inner)
		{
		}

		protected OperationAlreadyStartedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}

	public class ProgressStatistic
	{
		public enum EstimatingMethod
		{
			CurrentBytesPerSecond,
			AverageBytesPerSecond
		}

		private TimeSpan _currentBytesCalculationInterval = TimeSpan.FromSeconds(0.5);

		private readonly List<KeyValuePair<DateTime, long>> _currentBytesSamples = new List<KeyValuePair<DateTime, long>>()
		                                                                           {
			                                                                           new KeyValuePair<DateTime, long>(DateTime.MinValue, 0)
		                                                                           };

		//private KeyValuePair<DateTime, long>[] _currentBytesSamples = new KeyValuePair<DateTime, long>[6];

		private int _maxSampleCount;

		private EstimatingMethod _estimatingMethod = EstimatingMethod.CurrentBytesPerSecond;

		private readonly ProgressEventArgs _progressChangedArgs;

		public double AverageBytesPerSecond => BytesRead / Duration.TotalSeconds;
		
		public long BytesRead { get; private set; }

		public TimeSpan CurrentBytesCalculationInterval
		{
			get { return _currentBytesCalculationInterval; }
			set
			{
				if (HasStarted)
				{
					throw new InvalidOperationException("Task has already started!");
				}
				_currentBytesCalculationInterval = value;
			}
		}

		public double CurrentBytesPerSecond { get; private set; }

		public int CurrentBytesSampleCount
		{
			get { return _currentBytesSamples.Count; }
			set { _maxSampleCount = value; }
		}

		public TimeSpan Duration
		{
			get
			{
				if (!HasStarted)
				{
					return TimeSpan.Zero;
				}

				if (!HasFinished)
				{
					return DateTime.Now - StartingTime;
				}

				return FinishingTime - StartingTime;
			}
		}

		public TimeSpan EstimatedDuration
		{
			get
			{
				if (HasFinished)
				{
					return Duration;
				}
				if (TotalBytesToRead == -1)
				{
					return TimeSpan.MaxValue;
				}

				double bytesPerSecond;

				switch (UsedEstimatingMethod)
				{
					case EstimatingMethod.AverageBytesPerSecond:
						bytesPerSecond = AverageBytesPerSecond;
						break;
					case EstimatingMethod.CurrentBytesPerSecond:
						bytesPerSecond = CurrentBytesPerSecond;
						break;
					default:
						throw new ArgumentException(nameof(UsedEstimatingMethod));
				}

				var seconds = (TotalBytesToRead - BytesRead)/bytesPerSecond;
				if (seconds > 60*60*24*200 || double.IsNaN(seconds) || double.IsInfinity(seconds)) //over 200 Days -> infinite
				{
					return TimeSpan.MaxValue;
				}

				return Duration + TimeSpan.FromSeconds(seconds);
			}
		}

		public DateTime EstimatedFinishingTime
		{
			get
			{
				if (EstimatedDuration == TimeSpan.MaxValue)
				{
					return DateTime.MaxValue;
				}
				return StartingTime + EstimatedDuration;
			}
		}

		public DateTime FinishingTime { get; private set; }

		public bool HasFinished => FinishingTime != DateTime.MinValue;

		public bool HasStarted { get; private set; }

		public bool IsRunning => HasStarted && !HasFinished;

		public double Progress
		{
			get
			{
				if (TotalBytesToRead == -1)
				{
					return -1;
				}

				return BytesRead/(double) TotalBytesToRead;
			}
		}

		public DateTime StartingTime { get; private set; }

		public long TotalBytesToRead { get; private set; }

		public EstimatingMethod UsedEstimatingMethod
		{
			get { return _estimatingMethod; }
			set
			{
				if (HasStarted)
				{
					throw new OperationAlreadyStartedException();
				}
				_estimatingMethod = value;
			}
		}

		public ProgressStatistic()
		{
			StartingTime = DateTime.MinValue;
			FinishingTime = DateTime.MinValue;

			_progressChangedArgs = new ProgressEventArgs(this); //Event args can be cached
		}

		public event EventHandler<ProgressEventArgs> Finished;

		public event EventHandler<ProgressEventArgs> ProgressChanged;

		public event EventHandler<ProgressEventArgs> Started;

		public void Finish()
		{
			if (HasFinished) return;

			FinishingTime = DateTime.Now;
			OnFinished();
		}

		public void ProgressChange(long bytesRead, long totalBytesToRead)
		{
			if (bytesRead < BytesRead)
			{
				throw new ArgumentException("Operation cannot go backwards!", nameof(bytesRead));
			}

			if (HasFinished)
			{
				throw new InvalidOperationException("Operation has finished already!");
			}

			if (!HasStarted)
			{
				StartingTime = DateTime.Now;
				HasStarted = true;
				OnStarted();
			}

			BytesRead = bytesRead;
			TotalBytesToRead = totalBytesToRead;

			ProcessSample(bytesRead);

			OnProgressChanged();

			if (bytesRead != TotalBytesToRead) return;

			Finish();
			FinishingTime = DateTime.Now;
		}

		private void ProcessSample(long bytes)
		{
			var lastSample = _currentBytesSamples.Last();

			var now = DateTime.Now;

			if ((now - lastSample.Key).Ticks <= CurrentBytesCalculationInterval.Ticks / _maxSampleCount) return;

			var current = new KeyValuePair<DateTime, long>(now, bytes);

			_currentBytesSamples.Add(current);

			if (_currentBytesSamples.Count > _maxSampleCount)
			{
				_currentBytesSamples.RemoveAt(0);
			}

			if (lastSample.Key == DateTime.MinValue)
			{
				CurrentBytesPerSecond = AverageBytesPerSecond;
			}
			else
			{
				CurrentBytesPerSecond = (current.Value - lastSample.Value)/(current.Key - lastSample.Key).TotalSeconds;
			}
		}

		protected void OnFinished()
		{
			Finished?.Invoke(this, _progressChangedArgs);
		}

		protected void OnProgressChanged()
		{
			ProgressChanged?.Invoke(this, _progressChangedArgs);
		}

		protected void OnStarted()
		{
			Started?.Invoke(this, _progressChangedArgs);
		}
	}
}