﻿using System;
using System.IO;
using System.Threading;

namespace SS.FTPDownloader.Objects.Extensions
{
	public static class StreamHelpers
	{
		public static void CopyFrom(this Stream target, Stream source, CopyFromArguments args)
		{
			if (target == null)
			{
				throw new ArgumentNullException(nameof(target));
			}
			if (source == null)
			{
				throw new ArgumentNullException(nameof(source));
			}
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args));
			}
			
			var totalLength = args.TotalLength;
			if (totalLength == -1 && source.CanSeek)
			{
				totalLength = source.Length;
			}

			long lastLength = 0;

			var asyncResult = source.CopyToAsync(target);

			var timer = new Timer(
				state =>
				{
					if (args.ProgressChangeCallback == null || !target.CanWrite)
						return;

					var currentLength = target.Position;

					if (currentLength == (long) state)
						return;

					lastLength = currentLength;
					args.ProgressChangeCallback(currentLength, totalLength);
				},
				lastLength,
				TimeSpan.Zero,
				args.ProgressChangeCallbackInterval);

			while (!asyncResult.IsCompleted)
			{
				
			}

			timer.Dispose();

			args.ProgressChangeCallback?.Invoke(totalLength, totalLength);
		}
	}
}
