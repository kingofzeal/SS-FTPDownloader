﻿using System;
using SS.FTPDownloader.Utilities;

namespace SS.FTPDownloader.Objects
{
    public class DownloadedEventArgs : EventArgs
    {
	    public DownloadedEventArgs(DownloadedFile file, ProgressStatistic stats = null)
        {
            File = file;
            ProgressStatistics = stats;
        }

        public DownloadedFile File { get; }

        public ProgressStatistic ProgressStatistics { get; set; }
    }
}
