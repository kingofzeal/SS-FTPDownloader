﻿namespace SS.FTPDownloader.Objects.Media
{
	[Media(@"(?<Name>[a-z0-9 \.\-']+)(?!1080p)(?<Year>[0-9]{4}).*\.([a-z3-4]{3,4})\Z", 25)]
	[Media(@"(?<Name>[a-z0-9 \.\-']+) \((?<Year>[0-9]{4})\).*?\.([a-z3-4]{3,4})\Z", 25)]
	[Media(@"(?<Year>[0-9]{4}) - (?<Name>[a-z0-9 \.\-']+)\.([a-z3-4]{3,4})\Z", 26)]
	public class Movie : Media
	{
		public override string DisplayName => $"{Name} ({Year})";

		protected internal override string MoveLocation
		{
			get
			{
				var movieFolder = Name.ToCharArray()[Name.StartsWith("The ") ? 4 : 0];

				int result;
				if (int.TryParse(movieFolder.ToString(), out result))
				{
					movieFolder = '#';
				}

				return $@"{VideoRootPath}\Movies\{movieFolder}\{Name} ({Year}){Extension}";
			}
		}
		
		public override string TypeName => "Movie";

		public string Year { get; }

		public Movie() { }

		public Movie(string fileName, string videoRootPath = "") : base(fileName, videoRootPath)
		{
			var result = RegexMatch;

			if (result == null) return;

			Year = result.Groups["Year"].ToString();
		}
	}
}