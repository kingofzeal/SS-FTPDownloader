﻿using System;
using SS.FTPDownloader.Utilities;

namespace SS.FTPDownloader.Objects.Media
{
	[Media(@"(?<Name>[a-z0-9_ \.\-']+?)[\. ](?:- )?S(?:eason )?(?<Season>[0-9]{1,4}) ?E(?:pisode )?(?<Episode>[0-9]{1,3})((?:\-E)(?<EpisodeRange>[0-9]{1,3}))?.*\.([a-z3-4]{3,4})\Z", 100)]
	[Media(@"(?<Name>[a-z0-9_ \.\-']+?)[\.\- ](?:- )?(?<Season>[0-9]{1,4})x(?<Episode>[0-9]{1,3})(?:-(?<EpisodeRange>[0-9]{1,3}))?.*\.([a-z3-4]{3,4})\Z", 100)]
	[Media(@"(?<Name>[a-z0-9_ \.\-']+?)[\.\- ](?:- )?(?<Season>[0-9]{1,4}?)(?<Episode>[0-9]{1,3})(?:-(?<EpisodeRange>[0-9]{1,3}))?.*\.([a-z3-4]{3,4})\Z", 100)]
	[Media(@"(?<Name>[a-z0-9_ \.\-']+?)[\. ](?:- )?S(?:eason )?(?<Season>[0-9]{1,4}) - (?<Episode>[0-9]{1,3})((?:\-E)(?<EpisodeRange>[0-9]{1,3}))?.*\.([a-z3-4]{3,4})\Z", 105)]
	public class TvEpisode : Media
	{
		public override string DisplayName => $"{Name} - S{Season}E{Episode}";

		public string Episode { get; }

		public string EpisodeRange { get; }

		private string EpisodeRangeFormatted => string.IsNullOrEmpty(EpisodeRange) ? "" : $"-E{EpisodeRange}";

		protected internal override string MoveLocation =>
			$@"{VideoRootPath}\TV\{Name}\Season {Season}\{Name} - S{Season}E{Episode}{EpisodeRangeFormatted}{Extension}";

		public string Season { get; }

		protected override Func<string, string> RenamingFunction => TvRenamer.Rename;
		
		public override string TypeName => "TV Episode";
		
		public TvEpisode() { }

		public TvEpisode(string fileName, string videoRootPath = "")
			: base(fileName, videoRootPath)
		{
			var result = RegexMatch;

			if (result == null) return;
			
			var season = result.Groups["Season"].Value.TrimStart('0').Trim().PadLeft(2, '0');
			var episode = result.Groups["Episode"].Value.TrimStart('0').Trim().PadLeft(2, '0');
			var episodeRange = !string.IsNullOrEmpty(result.Groups["EpisodeRange"].Value) ?
				result.Groups["EpisodeRange"].Value.TrimStart('0').Trim().PadLeft(2, '0') :
				"";

			Season = season;
			Episode = episode;
			EpisodeRange = episodeRange;
		}
	}
}