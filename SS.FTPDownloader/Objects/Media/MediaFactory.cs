﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SS.FTPDownloader.Objects.Media
{
	public interface IMediaFactory
	{
		Media CreateMediaObject(string fileName, string videoRootPath = "", bool forceRefresh = false);
	}

	public class MediaFactory : IMediaFactory
	{
		private static List<ClassAttribute> _classAttributes;

		public Media CreateMediaObject(string fileName, string videoRootPath = "", bool forceRefresh = false)
		{
			if (_classAttributes == null || forceRefresh)
			{
				//Build (or rebuild) the type/attribute list
				_classAttributes = new List<ClassAttribute>();

				var typesWithAttribute = Assembly.GetExecutingAssembly()
					.GetTypes()
					.Where(type => type.IsDefined(typeof (MediaAttribute), true));

				/* 
					Because types can have multiple MediaAttributes on them, and each attribute
					may be ordered differently, break out each attribute and the matching type
					to its own object, which is added to the list.
				*/

				foreach (var type in typesWithAttribute)
				{
					foreach (var attribute in type.GetCustomAttributes(typeof (MediaAttribute), true))
					{
						_classAttributes.Add(new ClassAttribute
						{
							Attribute = (MediaAttribute) attribute,
							Type = type
						});
					}
				}
			}

			var matchingType = _classAttributes
				.OrderByDescending(x => x.Attribute.CheckOrder())
				.ThenBy(x => x.Type.Name)
				.FirstOrDefault(x => x.Attribute.MatchesMediaRegex(fileName));

			if (matchingType != null)
			{
				return (Media) Activator.CreateInstance(matchingType.Type, fileName, videoRootPath);
			}

			return null;
		}
	}
}
