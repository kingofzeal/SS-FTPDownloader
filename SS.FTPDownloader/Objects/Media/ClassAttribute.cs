﻿using System;

namespace SS.FTPDownloader.Objects.Media
{
	public class ClassAttribute
	{
		public Type Type { get; set; }
		public MediaAttribute Attribute { get; set; }
	}
}