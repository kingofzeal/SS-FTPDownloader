﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using log4net;
using SS.FTPDownloader.Utilities;

namespace SS.FTPDownloader.Objects.Media
{
	public abstract class Media
	{
		protected readonly string VideoRootPath;

		private static readonly ILog Log = LogManager.GetLogger(typeof(Media));

		/// <summary>
		/// How the object should be referred to for users when logging actions
		/// </summary>
		public abstract string DisplayName { get; }

		/// <summary>
		/// The file extension of the object
		/// </summary>
		public string Extension { get; }

		/// <summary>
		/// The full file name of the object
		/// </summary>
		public string FileName { get; }

		/// <summary>
		/// Where the object should be moved to when cleaning up
		/// </summary>
		protected internal abstract string MoveLocation { get; }

		private string _name;

		/// <summary>
		/// The name of the object after being run through any renaming functionality
		/// </summary>
		public string Name => GetName();

		/// <summary>
		/// The original name of the object
		/// </summary>
		protected string OriginalName { get; }
		
		/// <summary>
		/// The matched Regex information for the object
		/// </summary>
		protected Match RegexMatch { get; }

		/// <summary>
		/// A pointer to the function which should handle renaming the object, if necessary
		/// </summary>
		protected virtual Func<string, string> RenamingFunction => t => t;
		
		/// <summary>
		/// The type of object that should be presented to users when logging actions
		/// </summary>
		public abstract string TypeName { get; }
		
		internal Media() { }

		protected Media(string fileName, string videoRootPath = "")
		{
			VideoRootPath = videoRootPath;
			FileName = fileName;
			Extension = Path.GetExtension(fileName)?.ToLower();

			var attributes = (MediaAttribute[])Attribute.GetCustomAttributes(GetType(), typeof(MediaAttribute));

			Match result = null;

			var matchingAttribute = attributes
				.OrderByDescending(x => x.CheckOrder())
				.FirstOrDefault(x => x.MatchesMediaRegex(fileName));

			if (matchingAttribute != null)
				matchingAttribute.MatchesMediaRegex(fileName, out result);

			RegexMatch = result;

			if (!string.IsNullOrEmpty(RegexMatch?.Groups["Name"]?.Value))
			{
				OriginalName = 
					Thread
					.CurrentThread
					.CurrentCulture
					.TextInfo
					.ToTitleCase(
						RegexMatch
						.Groups["Name"]
						.ToString()
						.Replace(".", " ")
						.Replace("_", " "))
					.Trim();
			}
		}

		/// <summary>
		/// Generates what the name of the object should be, according to the renamingFunction provided. 
		/// If no renamingFunction is provided, this uses the RenamingFunction property of the object.
		/// </summary>
		/// <param name="renamingFunction">Optional. 
		/// A function which converts the original name of the object to what it should be.
		/// If no function is provided, the RenamingFunction propety is used instead</param>
		/// <param name="remapFile">Optional.
		/// The name of the file that provides the remapping data.</param>
		/// <returns>The name of the object after being processed by the renamingFunction</returns>
		internal string GetName(Func<string, string> renamingFunction = null)
		{
			if (!string.IsNullOrEmpty(_name))
				return _name;

			if (renamingFunction == null)
				renamingFunction = RenamingFunction;
			
			_name = renamingFunction(OriginalName);

			return _name;
		}

		/// <summary>
		/// Moves the object to the location specified by the MoveLocation property, creating directories as needed.
		/// </summary>
		/// <param name="fileUtility">An instance of an IFileUtilities object</param>
		public void Process(IFileUtilities fileUtility)
		{
			fileUtility.MakeDirs(MoveLocation);
			fileUtility.Move(FileName, MoveLocation);
			
			Log.Info($"{DisplayName} found, moved successfully.");
		}
	}
}