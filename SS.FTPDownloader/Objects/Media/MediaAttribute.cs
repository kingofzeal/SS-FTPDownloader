﻿using System;
using System.Text.RegularExpressions;

namespace SS.FTPDownloader.Objects.Media
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
	public sealed class MediaAttribute : Attribute
	{
		private readonly string _mediaRegexPattern;
		private readonly int _checkOrder;

		public MediaAttribute(string mediaRegexPattern, int checkOrder = 50)
		{
			_mediaRegexPattern = mediaRegexPattern;
			_checkOrder = checkOrder;
		}

		public bool MatchesMediaRegex(string fileName)
		{
			Match result;

			return MatchesMediaRegex(fileName, out result);
		}

		public int CheckOrder() => _checkOrder;

		public bool MatchesMediaRegex(string fileName, out Match regexMatch)
		{
			regexMatch = null;

			var match = Regex.Match(fileName, _mediaRegexPattern, RegexOptions.IgnoreCase);

			if (!match.Success) return false;

			regexMatch = match;
			return true;
		}
	}
}
