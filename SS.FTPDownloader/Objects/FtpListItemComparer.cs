﻿using System.Collections.Generic;
using System.Net.FtpClient;

namespace SS.FTPDownloader.Objects
{
    public class FtpListItemComparer : IEqualityComparer<FtpListItem>
    {
        #region IEqualityComparer<FtpListItem> Members

        public bool Equals(FtpListItem x, FtpListItem y)
        {
            return x.FullName == y.FullName;
        }

        public int GetHashCode(FtpListItem obj)
        {
            return obj.FullName.GetHashCode();
        }

        #endregion
    }
}
