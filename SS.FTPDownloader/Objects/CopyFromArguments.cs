﻿using System;

namespace SS.FTPDownloader.Objects
{
    public class CopyFromArguments
    {
        public delegate void ProgressChange(long bytesRead, long totalBytesToRead);
		
        public ProgressChange ProgressChangeCallback { get; }

        public TimeSpan ProgressChangeCallbackInterval { get; }
		
        public long TotalLength { get; }

        public CopyFromArguments() : this(null) { }

        public CopyFromArguments(ProgressChange progressChangeCallback)
            : this(progressChangeCallback, TimeSpan.FromSeconds(0.2))
        { }

	    public CopyFromArguments(ProgressChange progressChangeCallback, TimeSpan progressChangeCallbackInterval, long totalLength = -1)
        {
			if (progressChangeCallbackInterval.TotalSeconds < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(progressChangeCallbackInterval),
					"ProgressChangeCallbackInterval has to be greater or equal to 0");
			}

			ProgressChangeCallback = progressChangeCallback;
            ProgressChangeCallbackInterval = progressChangeCallbackInterval;
            TotalLength = totalLength;
        }
    }
}
