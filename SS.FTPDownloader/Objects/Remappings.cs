﻿using System;
using System.Collections.Generic;

namespace SS.FTPDownloader.Objects
{
    public class Remappings
    {
        public Remappings()
        {
            Timestamp = DateTime.MinValue;
            Mappings = new Dictionary<string, string>();
        }

        public DateTime Timestamp { get; set; }

        public Dictionary<string, string> Mappings { get; set; }
    }
}
