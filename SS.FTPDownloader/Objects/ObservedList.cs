﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SS.FTPDownloader.Objects
{
    public class ObservedList<T> : List<T>
    {
        public event EventHandler<ObservedListEventArgs> OnAdd;
        public event EventHandler<ObservedListEventArgs> OnRemove;

        public new void Add(T item)
        {
	        OnAdd?.Invoke(this, new ObservedListEventArgs(item));

	        base.Add(item);
        }

        public new void Remove(T item)
        {
	        OnRemove?.Invoke(this, new ObservedListEventArgs(item));

	        base.Remove(item);
        }

        public new void AddRange(IEnumerable<T> listItems)
        {
	        var enumerable = listItems as IList<T> ?? listItems.ToList();

            foreach (var item in enumerable)
            {
	            OnAdd?.Invoke(this, new ObservedListEventArgs(item));
            }

	        base.AddRange(enumerable);
        }
    }

    public class ObservedListEventArgs : EventArgs
    {
        public ObservedListEventArgs(object item)
        {
            Item = item;
        }

        public object Item { get; }
    }

}
