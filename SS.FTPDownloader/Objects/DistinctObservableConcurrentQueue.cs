﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace SS.FTPDownloader.Objects
{
	public class DistinctObservableConcurrentQueue<T> : ConcurrentQueue<T>
	{
		public event EventHandler<ObservedListEventArgs> OnAdd;
		public event EventHandler<ObservedListEventArgs> OnRemove;

		public void Enqueue(T item, IEqualityComparer<T> comparer)
		{
			if (!this.Any(x => comparer.Equals(item)))
			{
				Enqueue(item);
			}
		}

		public new void Enqueue(T item)
		{
			OnAdd?.Invoke(this, new ObservedListEventArgs(item));

			base.Enqueue(item);
		}

		public new bool TryDequeue(out T item)
		{
			var result = base.TryDequeue(out item);

			OnRemove?.Invoke(this, new ObservedListEventArgs(item));

			return result;
		}

		public void AddRange(IEnumerable<T> listItems)
		{
			var enumerable = listItems as IList<T> ?? listItems.ToList();

			foreach (var item in enumerable)
			{
				Enqueue(item);
			}
		}

		public void AddRange(IEnumerable<T> listItems, IEqualityComparer<T> comparer)
		{
			var enumerable = listItems as IList<T> ?? listItems.ToList();

			foreach (var item in enumerable)
			{
				Enqueue(item, comparer);
			}
		}
	}

	public class ObservedListEventArgs : EventArgs
	{
		public ObservedListEventArgs(object item)
		{
			Item = item;
		}

		public object Item { get; }
	}

}
