﻿using System.Net.FtpClient;

namespace SS.FTPDownloader.Objects
{
    public class DownloadedFile
    {
        public FtpListItem FtpListItem { get; set; }
        public string DownloadLocation { get; set; }
    }
}
