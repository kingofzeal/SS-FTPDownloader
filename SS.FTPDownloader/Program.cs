﻿using System.Windows.Forms;

namespace SS.FTPDownloader
{
    internal class Program
    {
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AutoDownloadForm());
        }
    }
}