# FTP Downloader #

## Requirements ##
- Credentials to connect to the FTP server above

### FTP Server ###
- Server should be accessible via port 21
- Server should accept SSL connections
- Credentials used to log into the server should allow for listing of directories, and downloading and deleting files on the server
- An optional folder can be created on the FTP server that contains the files to be downloaded and removed [FTPRootCheckDir]

### Local Machine ##
- Directories on the local machine should be created:
	- A directory to hold the currently-downloading files, or files whose media types cannot be determined [TempDownloadPath]
    - A directory to hold the finished and identified files [VideoRootPath]
    	- An additional subfolder named "TV" should be created under the **VideoRootPath**

## Config Settings ##
- **ExpeditedRecheckTime** - Not currently used.
- **FTPPassword** - The password for the username to connect to the FTP server
- **FTPRootCheckDir** - The remote directory the client should check for new files. Will automatically iterate - **FTPUrl** - The URL to the FTP server. The 'FTP://' prefix is not required.
- **FTPUserName** - The username used to connect to the FTP server
- **MaxRetryCount** - The maximum number of attempts to download a specific file. Once reached, the file will be - **NormalRecheckTime** - How often the client should log into the FTP server and check for new files.
through all nested folders.
- **RemapFile** - The location of the Remap CSV file.
- **TempDownloadPath** - The local directory files are downloaded to before sorting.
- **VideoRootPath** - The local directory files are moved to once downloaded and identified.
removed from the download queue. Note this does not remove the file from the server, and may be re-added to the download queue on the next check.

## Execution
1. The client connects to the FTP server with the given credentials and checks the **[FTPRootCheckDir]** (and subfolders) for any files. If any files are found the details of all the files are saved, and the client disconnects. This process is repeated every **[NormalRecheckTime]** minutes.
2. If files are found, the client waits for 5 seconds and reconnects to the server. A new list of files are obtained and the last modified timestamp on the files are compared to the original list. If the timestamp has not changed, and the file is not currently in the download queue, the file is added to the download queue.
3. If any files are added to the download queue, the client begins downloading them in order, one at a time, to the **[TempDownloadPath]** folder. If any errors occur during downloading, the file is re-added to the end of the queue. If a file has more than **[MaxRetryErrors]** errors, the file is removed from the queue. When the file is finished downloading, it is deleted from the FTP server, the file information is passed to a cleanup process, and the next file in the list begins downloading.
4. When file information is passed to the cleanup process, the file name is parsed against several RegExes to determine which type of media it is. If the type can be determined from the file name, it is renamed to a standard scheme and moved to an appropriate folder. If the appropriate folder does not exist, it is created. If the type cannot be determined from the file name, it is left in the **[TempDownloadPath]** folder.