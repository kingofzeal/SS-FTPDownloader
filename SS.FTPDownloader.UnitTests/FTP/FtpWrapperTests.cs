﻿using System.Net.FtpClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using SS.FTPDownloader.FTP;
using SS.FTPDownloader.Objects;
using SS.FTPDownloader.Utilities;

namespace SS.FTPDownloader.UnitTests.FTP
{
	[TestClass]
	public class FtpWrapperTests
	{
		private IFtpClient _ftpClient;
		private IFtpWrapper _ftpWrapper;

		[TestCleanup]
		public void Cleanup()
		{
		}

		[TestInitialize]
		public void Initialize()
		{
			_ftpClient = MockRepository.GenerateMock<IFtpClient>();
			_ftpWrapper = new FtpWrapper(_ftpClient, new Settings());
		}

		[TestMethod]
		public void ConnectStartsConnectionAndSetsWorkingDirectory()
		{
			//Arrange
			_ftpClient.Expect(x => x.Connect());
			_ftpClient.Expect(x => x.SetWorkingDirectory("unpacked"));

			//Act
			_ftpWrapper.Connect();

			//Assert
			//_ftpClient.VerifyAllExpectations();
		}

		[TestMethod]
		public void DeleteFileCallsClientDelete()
		{
			//Arrange
			var file = new FtpListItem
					   {
						   FullName = ""
					   };

			_ftpClient.Expect(x => x.DeleteFile(file.FullName));

			//Act
			_ftpWrapper.DeleteFile(file);

			//Assert
			_ftpClient.VerifyAllExpectations();
		}
	}

}
