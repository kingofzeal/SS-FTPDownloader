﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using SS.FTPDownloader.Objects.Media;
using SS.FTPDownloader.Utilities;

// ReSharper disable once CheckNamespace
namespace SS.FTPDownloader.UnitTests.Objects.TvEpisodeTests
{
	[TestClass]
	public class FileNameParsingTests
	{
		[TestMethod]
		public void TvEpisodesWithSceneFileNameFormattingAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "The.Big.Bang.Theory.S04E03.The.Zazzy.Substitution.BluRay.1080p.Remux.AVC.DTS-HDMA.5.1-BluHD.mkv";

			var expectedResults = new
			{
				FileName = "The.Big.Bang.Theory.S04E03.The.Zazzy.Substitution.BluRay.1080p.Remux.AVC.DTS-HDMA.5.1-BluHD.mkv",
				ShowName = "The Big Bang Theory",
				Extension = ".mkv",
				Episode = "03",
				EpisodeRange = "",
				Season = "04"
			};

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.ShowName, obj.Name);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.Episode, obj.Episode);
			Assert.AreEqual(expectedResults.EpisodeRange, obj.EpisodeRange);
			Assert.AreEqual(expectedResults.Season, obj.Season);
		}


		[TestMethod]
		public void TvEpisodesWithAnXSeasonEpisodeSeparatorAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "doctor_who_2005.8x08.mummy_on_the_orient_express.720p_hdtv_x264-fov.mkv";

			var expectedResults = new
			{
				FileName = "doctor_who_2005.8x08.mummy_on_the_orient_express.720p_hdtv_x264-fov.mkv",
				ShowName = "Doctor Who 2005",
				Extension = ".mkv",
				Episode = "08",
				EpisodeRange = "",
				Season = "08"
			};

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.ShowName, obj.Name);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.Episode, obj.Episode);
			Assert.AreEqual(expectedResults.EpisodeRange, obj.EpisodeRange);
			Assert.AreEqual(expectedResults.Season, obj.Season);
		}

		[TestMethod]
		public void TvEpisodesAlreadyInTargetFormattingAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "Doctor Who - S09E16.mkv";

			var expectedResults = new
			{
				FileName = "Doctor Who - S09E16.mkv",
				ShowName = "Doctor Who",
				Extension = ".mkv",
				Episode = "16",
				EpisodeRange = "",
				Season = "09"
			};

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.ShowName, obj.Name);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.Episode, obj.Episode);
			Assert.AreEqual(expectedResults.EpisodeRange, obj.EpisodeRange);
			Assert.AreEqual(expectedResults.Season, obj.Season);
		}

		[TestMethod]
		public void TvEpisodesWithSceneFileNameFormattingAndARangeOfEpisodesAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "The.Big.Bang.Theory.S04E03-E05.mkv";

			var expectedResults = new
			{
				FileName = "The.Big.Bang.Theory.S04E03-E05.mkv",
				ShowName = "The Big Bang Theory",
				Extension = ".mkv",
				Episode = "03",
				EpisodeRange = "05",
				Season = "04"
			};

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.ShowName, obj.Name);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.Episode, obj.Episode);
			Assert.AreEqual(expectedResults.EpisodeRange, obj.EpisodeRange);
			Assert.AreEqual(expectedResults.Season, obj.Season);
		}

		[TestMethod]
		public void TvEpisodesAlreadyInTargetFormattingAndARangeOfEpisodesAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "Doctor Who - S09E06-E09.mkv";

			var expectedResults = new
			{
				FileName = "Doctor Who - S09E06-E09.mkv",
				ShowName = "Doctor Who",
				Extension = ".mkv",
				Episode = "06",
				EpisodeRange = "09",
				Season = "09"
			};

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.ShowName, obj.Name);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.Episode, obj.Episode);
			Assert.AreEqual(expectedResults.EpisodeRange, obj.EpisodeRange);
			Assert.AreEqual(expectedResults.Season, obj.Season);
		}

		[TestMethod]
		public void TvEpisodesWithSpelledOutSeasonAndEpisodeAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "Farscape Season 01 Episode 01 - Through the Eye of the Needle TSV.mkv";

			var expectedResults = new
			{
				FileName = "Farscape Season 01 Episode 01 - Through the Eye of the Needle TSV.mkv",
				ShowName = "Farscape",
				Extension = ".mkv",
				Episode = "01",
				Season = "01"
			};

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.ShowName, obj.Name);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.Episode, obj.Episode);
			Assert.AreEqual(expectedResults.Season, obj.Season);
		}

		[TestMethod]
		public void TvEpisodesWithAnXSeasonEpisodeSeperatorAndRangeOfEpisodesAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "las.vegas-05x01-02.avi";

			var expectedResults = new
			{
				FileName = "las.vegas-05x01-02.avi",
				ShowName = "Las Vegas",
				Extension = ".avi",
				Episode = "01",
				EpisodeRange = "02",
				Season = "05"
			};

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.ShowName, obj.Name);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.Episode, obj.Episode);
			Assert.AreEqual(expectedResults.EpisodeRange, obj.EpisodeRange);
			Assert.AreEqual(expectedResults.Season, obj.Season);
		}

		[TestMethod]
		public void TvEpisodesWithNoSeasonEpisodeSeperatorAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "Las Vegas 304 - Whatever Happened to Seymour Magoon.avi";

			var expectedResults = new
			{
				FileName = "Las Vegas 304 - Whatever Happened to Seymour Magoon.avi",
				ShowName = "Las Vegas",
				Extension = ".avi",
				Episode = "04",
				//EpisodeRange = "02",
				Season = "03"
			};

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.ShowName, obj.Name);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.Episode, obj.Episode);
			Assert.AreEqual(expectedResults.Season, obj.Season);
		}

		[TestMethod]
		public void TvEpisodesWithSeasonButNoEpisodeIdentifierAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "The Simpsons Season 24 - 10.mp4";

			var expectedResults = new
			{
				FileName = "The Simpsons Season 24 - 10.mp4",
				ShowName = "The Simpsons",
				Extension = ".mp4",
				Episode = "10",
				//EpisodeRange = "02",
				Season = "24"
			};

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.ShowName, obj.Name);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.Episode, obj.Episode);
			Assert.AreEqual(expectedResults.Season, obj.Season);
		}
	}

	[TestClass]
	public class FileDetectionTests
	{

		[TestMethod]
		public void TvShowsWithSceneFileNameAreDetectedAsTvEpisodes()
		{
			//Arrange
			var fileName = "The.Big.Bang.Theory.S04E03.The.Zazzy.Substitution.BluRay.1080p.Remux.AVC.DTS-HDMA.5.1-BluHD.mkv";

			//Act
			var result = new TvEpisode(fileName);

			//Assert
			Assert.IsTrue(!string.IsNullOrEmpty(result.Name));
		}

		[TestMethod]
		public void MoviesWithSceneFileNAmesAreNotDetectedAsTvEpisodes()
		{
			//Arrange
			var fileName = "Transformers.Age.Of.Extinction.2014.1080p.DVDR.NTSC-LPD.mkv";

			//Act
			var result = new TvEpisode(fileName);

			//Assert
			Assert.IsFalse(!string.IsNullOrEmpty(result.Name));
		}
	}

	[TestClass]
	public class FileMovingTests
	{
		[TestMethod]
		public void TvEpisodeIsMovedCorrectlyBasedOnFileName()
		{
			//Arrange
			var fileUtility = MockRepository.GenerateMock<IFileUtilities>();
			var episode = new TvEpisode("Show Name - S01E02.avi");

			fileUtility.Expect(x => x.Move(episode.FileName, @"D:\Video\\TV\Show Name\Season 01\Show Name - S01E02.avi"));

			//Act
			episode.Process(fileUtility);

			//Assert
			//fileUtility.VerifyAllExpectations();
		}

		[TestMethod]
		public void TvEpisodeMoveLocationProperlyAppendsRangeIfItExists()
		{
			//Arrange
			var fileName = "Doctor Who - S09E06-E09.mkv";

			var expectedResult = "\\TV\\Doctor Who\\Season 09\\Doctor Who - S09E06-E09.mkv";

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResult, obj.MoveLocation);
		}

		[TestMethod]
		public void TvEpisodeMoveLocationIsTheCorrectPath()
		{
			//Arrange
			var fileName = "Doctor Who - S09E06.mkv";

			var expectedResult = "\\TV\\Doctor Who\\Season 09\\Doctor Who - S09E06.mkv";

			//Act
			var obj = new TvEpisode(fileName);

			//Assert
			Assert.AreEqual(expectedResult, obj.MoveLocation);
		}
	}

	[TestClass]
	public class FileRenamingTests
	{
		[TestMethod]
		public void TvEpisodesAreRenamedAccordingToRenameList()
		{
			//Arrange
			var episode = new TvEpisode("Show Name - S01E02.avi");
			const string expectedShowName = "New Show";


			//Act
			var result = episode.GetName(s => s == "Show Name" ? expectedShowName : s);

			//Assert 
			Assert.AreEqual(expectedShowName, result);
		}
	}

	[TestClass]
	public class FolderCreationTests
	{
		[TestMethod]
		public void TvEpisodeAttemptsToMakeCorrectFolderPaths()
		{
			//Arrange
			var fileUtility = MockRepository.GenerateMock<IFileUtilities>();
			var episode = new TvEpisode("Show Name - S01E02.avi");

			fileUtility.Expect(x => x.MakeDirs(@"D:\Video\\TV\Show Name\Season 01\Show Name - S01E02.avi"));

			//Act
			episode.Process(fileUtility);

			//Assert 
			//fileUtility.VerifyAllExpectations();
		}
	}
}
