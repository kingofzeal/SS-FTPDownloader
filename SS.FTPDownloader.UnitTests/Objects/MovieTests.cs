﻿using SS.FTPDownloader.Objects.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable once CheckNamespace
namespace SS.FTPDownloader.UnitTests.Objects.MovieTests
{
	// ReSharper disable once ClassNeverInstantiated.Global
	[TestClass]
	public class FileNameParsingTests
	{
		[TestMethod]
		public void MoviesFormattedWithSceneFormattingAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "Transformers.Age.Of.Extinction.2014.DVDR.NTSC-LPD.mkv";

			var expectedResults = new
			{
				FileName = "Transformers.Age.Of.Extinction.2014.DVDR.NTSC-LPD.mkv",
				MovieName = "Transformers Age Of Extinction",
				Extension = ".mkv",
				Year = "2014"
			};

			//Act
			var obj = new Movie(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.MovieName, obj.Name);
			Assert.AreEqual(expectedResults.Year, obj.Year);
		}

		[TestMethod]
		public void MoviesFormattedWithSceneFormattingAnd1080PEncodingAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "Transformers.Age.Of.Extinction.2014.1080p.DVDR.NTSC-LPD.mkv";

			var expectedResults =
				new
				{
					FileName = "Transformers.Age.Of.Extinction.2014.1080p.DVDR.NTSC-LPD.mkv",
					MovieName = "Transformers Age Of Extinction",
					Extension = ".mkv",
					Year = "2014"
				};

			//Act
			var obj = new Movie(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.MovieName, obj.Name);
			Assert.AreEqual(expectedResults.Year, obj.Year);
		}

		[TestMethod]
		public void MoviesAlreadyInTargetFormattingAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "Toy Story 2 (1999).avi";

			var expectedResults = new
			{
				FileName = "Toy Story 2 (1999).avi",
				MovieName = "Toy Story 2",
				Extension = ".avi",
				Year = "1999"
			};

			//Act
			var obj = new Movie(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.MovieName, obj.Name);
			Assert.AreEqual(expectedResults.Year, obj.Year);
		}

		[TestMethod]
		public void MoviesAlreadyInTargetFormattingWithExtraDataAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "Bleach Fade to Black (2008) [720p] [h.265].mkv";

			var expectedResults = new
			{
				FileName = "Bleach Fade to Black (2008) [720p] [h.265].mkv",
				MovieName = "Bleach Fade To Black",
				Extension = ".mkv",
				Year = "2008"
			};

			//Act
			var obj = new Movie(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.MovieName, obj.Name);
			Assert.AreEqual(expectedResults.Year, obj.Year);
		}

		[TestMethod]
		public void MoviesWithTheYearPreceedingTheNameAreCorrectlyParsed()
		{
			//Arrange
			var fileName = "2008 - Wall-E.avi";

			var expectedResults = new
			{
				FileName = "2008 - Wall-E.avi",
				MovieName = "Wall-E",
				Extension = ".avi",
				Year = "2008"
			};

			//Act
			var obj = new Movie(fileName);

			//Assert
			Assert.AreEqual(expectedResults.FileName, obj.FileName);
			Assert.AreEqual(expectedResults.Extension, obj.Extension);
			Assert.AreEqual(expectedResults.MovieName, obj.Name);
			Assert.AreEqual(expectedResults.Year, obj.Year);
		}
	}

	[TestClass]
	public class FileDetectionTests
	{
		[TestMethod]
		public void MoviesWithSceneFileNamesAreDetectedAsMovies()
		{
			//Arrange
			var fileName = "Transformers.Age.Of.Extinction.2014.1080p.DVDR.NTSC-LPD.mkv";

			//Act
			var result = new Movie(fileName);

			//Assert
			Assert.IsTrue(!string.IsNullOrEmpty(result.Name));
		}

		[TestMethod]
		public void TvShowsWithSceneFileNamesAreNotDetectedAsMovies()
		{
			//Arrange
			var fileName = "The.Big.Bang.Theory.S04E03.The.Zazzy.Substitution.BluRay.1080p.Remux.AVC.DTS-HDMA.5.1-BluHD.mkv";

			//Act
			var result = new Movie(fileName);

			//Assert
			Assert.IsFalse(!string.IsNullOrEmpty(result.Name));
		}
	}

	[TestClass]
	public class FileMovingTests
	{
		[TestMethod]
		public void MoviesThatStartWithANumberAreMovedToTheGenericNumberFolder()
		{
			//Arrange
			var fileName = "2009 (2012).avi";

			//Act
			var result = new Movie(fileName);

			//Assert
			Assert.AreEqual("\\Movies\\#\\2009 (2012).avi", result.MoveLocation);
		}

		[TestMethod]
		public void MoviesThatStartWithALetterAreMovedToTheFolderOfTheFirstLetter()
		{
			//Arrange
			var fileName = "Idiocracy (2006).avi";

			//Act
			var result = new Movie(fileName);

			//Assert
			Assert.AreEqual("\\Movies\\I\\Idiocracy (2006).avi", result.MoveLocation);
		}

		[TestMethod]
		public void MoviesThatStartWithTheAreMovedToTheFolderOfTheFirstLetterOfTheFirstWordAfterThe()
		{
			//Arrange
			var fileName = "The Shining (1980).avi";

			//Act
			var result = new Movie(fileName);

			//Assert
			Assert.AreEqual("\\Movies\\S\\The Shining (1980).avi", result.MoveLocation);
		}
	}


	/*
			[TestMethod]
			public void MovieAttemptsToMakeCorrectFolderPaths()
			{

			}*/
}
