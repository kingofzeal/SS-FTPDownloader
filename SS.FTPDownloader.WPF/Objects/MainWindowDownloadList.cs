using System;
using System.ComponentModel;
using System.Net.FtpClient;
using System.Runtime.CompilerServices;
using SS.FTPDownloader.Annotations;

namespace SS.FTPDownloader.WPF.Objects
{
	public class MainWindowDownloadList : INotifyPropertyChanged
	{
		private string _parsedName;
		private string _parsedType;
		private DateTime _lastUpdated;
		private string _status;
		public FtpListItem Item { get; set; }

		public string Status
		{
			get { return _status; }
			set
			{
				if (value == _status) return;
				_status = value;
				OnPropertyChanged();
			}
		}

		public DateTime LastUpdated
		{
			get { return _lastUpdated; }
			set
			{
				if (value.Equals(_lastUpdated)) return;
				_lastUpdated = value;
				OnPropertyChanged();
			}
		}

		public string ParsedType
		{
			get { return _parsedType; }
			set
			{
				if (value == _parsedType) return;
				_parsedType = value;
				OnPropertyChanged();
			}
		}

		public string ParsedName
		{
			get { return _parsedName; }
			set
			{
				if (value == _parsedName) return;
				_parsedName = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			if (propertyName != "LastUpdated")
			{
				LastUpdated = DateTime.Now;
			}

			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}