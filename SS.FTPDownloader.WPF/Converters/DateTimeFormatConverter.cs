﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SS.FTPDownloader.WPF.Converters
{
	public class DateTimeFormatConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return (DateTime) value == DateTime.MinValue ? string.Empty : ((DateTime) value).ToString((string) parameter);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
