﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SS.FTPDownloader.WPF.Converters
{
	public class SpeedConverter : IValueConverter
	{
		static readonly string[] SizeSuffixes = { "bits", "Kb", "Mb", "Gb", "Tb", "Pb", "Eb", "Zb", "Yb" };

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var bitsPerSecond = (long) value * 8;

			if (bitsPerSecond == 0)
			{
				return "N/A";
			}

			var mag = (int)Math.Log(bitsPerSecond, 1024);
			var adjustedSize = (decimal)bitsPerSecond / (1L << (mag * 10));

			return $"{adjustedSize:n1} {SizeSuffixes[mag]}/s";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
