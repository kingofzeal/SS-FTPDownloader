﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Net.FtpClient;
using System.Windows;
using System.Windows.Controls;
using SS.FTPDownloader.FTP;
using SS.FTPDownloader.Objects;
using SS.FTPDownloader.Objects.Media;
using SS.FTPDownloader.Utilities;
using SS.FTPDownloader.WPF.Objects;
using SS.FTPDownloader.WPF.ViewModels;

namespace SS.FTPDownloader.WPF
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private readonly FtpDownloader _downloader;
		private readonly MainViewModel _viewModel;
		private MediaFactory _factory;

		public MainWindow()
		{
			InitializeComponent();

			_viewModel = new MainViewModel();

			DataContext = _viewModel;

			var settings = new Settings
			{
				NormalRecheckTime = Properties.Settings.Default.NormalRecheckTime,
				ExpeditedRecheckTime = Properties.Settings.Default.ExpeditedRecheckTime,
				FtpRootCheckDir = Properties.Settings.Default.FTPRootCheckDir,
				TempDownloadPath = Properties.Settings.Default.TempDownloadPath,
				RemapFile = Properties.Settings.Default.RemapFile,
				MaxRetryCount = Properties.Settings.Default.MaxRetryCount,
				ProgressBarSize = Properties.Settings.Default.ProgressBarSize,
				VideoRootPath = Properties.Settings.Default.VideoRootPath,
				BatchDownloads = Properties.Settings.Default.BatchDownloads,
				FtpUserName = Properties.Settings.Default.FTPUserName,
				FtpPassword = Properties.Settings.Default.FTPPassword,
				FtpUrl = Properties.Settings.Default.FTPUrl,
				BatchDownloadSize = Properties.Settings.Default.BatchDownloadSize
			};

			_downloader = new FtpDownloader(settings, new FtpEvents
			                                        {
				                                        UpdatedProgressHandler = updateProgressBar,
				                                        DownloadStartedHandler = downloadStarted,
														DownloadErrorHandler = downloadError,
				                                        DownloadCompletedHandler = downloadCompleted,
				                                        CleanupCompletedHandler = cleanupCompleted,
				                                        CleanupStartedHandler = cleanupStarted
			                                        });
			
			FtpWrapper.DownloadList.OnAdd += downloadListOnOnAdd;

			_downloader.BeginProcess();
		}

		private void cleanupStarted(object sender, FileCleanupEventArgs e)
		{
			foreach (var listItem in _viewModel.DownloadList.Where(x => x.Item.FullName == e.File.FtpListItem.FullName))
			{
				listItem.Status = "Moving";
			}
		}

		private void cleanupCompleted(object sender, FileCleanupEventArgs e)
		{
			foreach (var listItem in _viewModel.DownloadList.Where(x => x.Item.FullName == e.File.FtpListItem.FullName))
			{
				listItem.Status = "Moved";
			}
		}

		private void downloadCompleted(object sender, DownloadedEventArgs e)
		{
			foreach (var listItem in _viewModel.DownloadList.Where(x => x.Item.FullName == e.File.FtpListItem.FullName))
			{
				listItem.Status = "Downloaded";
			}
		}

		private void downloadError(object sender, DownloadedEventArgs e)
		{
			foreach (var listItem in _viewModel.DownloadList.Where(x => x.Item.FullName == e.File.FtpListItem.FullName))
			{
				listItem.Status = "Error";
			}
		}

		private void downloadStarted(object sender, DownloadedEventArgs e)
		{
			foreach (var listItem in _viewModel.DownloadList.Where(x => x.Item.FullName == e.File.FtpListItem.FullName))
			{
				listItem.Status = "Downloading";
			}
		}
		
		private void downloadListOnOnAdd(object sender, ObservedListEventArgs observedListEventArgs)
		{
			var item = observedListEventArgs.Item as FtpListItem;

			if (item != null && _viewModel.DownloadList.All(x => x.Item.FullName != item.FullName))
			{
				if (_factory == null)
				{
					_factory = new MediaFactory();
				}

			    try
			    {
			        var media = _factory.CreateMediaObject(item.Name);

			        Application.Current.Dispatcher.Invoke(delegate
			                                              {
			                                                  _viewModel.DownloadList.Add(new MainWindowDownloadList
			                                                                              {
			                                                                                  Item = item,
			                                                                                  Status = "Found",
			                                                                                  ParsedName = media.DisplayName,
			                                                                                  ParsedType = media.TypeName
			                                                                              });
			                                              });
			    }
			    catch
			    {
			        Application.Current.Dispatcher.Invoke(delegate
			                                              {
			                                                  _viewModel.DownloadList.Add(new MainWindowDownloadList
			                                                                              {
			                                                                                  Item = item,
			                                                                                  Status = "Found",
			                                                                                  ParsedName = "",
			                                                                                  ParsedType = ""
			                                                                              });
			                                              });
			    }
			}

			foreach (var expiredItem in _viewModel.DownloadList.Where(x => x.LastUpdated <= DateTime.Now.AddDays(-1) && x.Status == "Moved").ToList())
			{
				Application.Current.Dispatcher.Invoke(delegate
				                                      {
					                                      _viewModel.DownloadList.Remove(expiredItem);
				                                      });
			}
		}
		
		private void updateProgressBar(object sender, ProgressEventArgs progressEventArgs)
		{
			if (progressEventArgs == null)
			{
				return;
			}

			var stat = progressEventArgs.ProgressStatistic;

			if (stat.HasFinished || double.IsNaN(stat.Progress))
			{
				_viewModel.ProgressCompleted = 0;
				_viewModel.EstimatedCompletionTime = DateTime.MinValue;
				_viewModel.AverageSpeed = 0;
				_viewModel.CurrentSpeed = 0;
				return;
			}

			_viewModel.ProgressCompleted = (decimal) (stat.Progress * _viewModel.ProgressBarResolution);
			_viewModel.EstimatedCompletionTime = stat.EstimatedFinishingTime;
			_viewModel.AverageSpeed = (long) stat.AverageBytesPerSecond;
			_viewModel.CurrentSpeed = (long) stat.CurrentBytesPerSecond;
		}

		private void LogBox_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			LogBox.Focus();
			LogBox.CaretIndex = LogBox.Text.Length;
			LogBox.ScrollToEnd();
		}
	}
}
