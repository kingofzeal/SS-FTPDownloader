using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using log4net;
using log4net.Core;
using log4net.Repository.Hierarchy;
using SS.FTPDownloader.Annotations;
using SS.FTPDownloader.Utilities;
using SS.FTPDownloader.WPF.Objects;

namespace SS.FTPDownloader.WPF.ViewModels
{
	public class MainViewModel : INotifyPropertyChanged
	{
		private string _log;
		private decimal _progressCompleted;
		private DateTime _estimatedCompletionTime;
		private long _currentSpeed;
		private long _averageSpeed;
		private WpfAppender _appender;

		public MainViewModel()
		{
			DownloadList = new ObservableCollection<MainWindowDownloadList>();
			Appender = (WpfAppender) LoggerManager.GetAllRepositories()
				?.First()
				?.GetAppenders()
				?.First(x => x.GetType() == typeof (WpfAppender));
		}

		public ObservableCollection<MainWindowDownloadList> DownloadList { get; set; }

		public string Log
		{
			get { return _log; }
			set
			{
				if (value == _log) return;
				_log = value;
				OnPropertyChanged();
			}
		}

		public decimal ProgressCompleted
		{
			get { return _progressCompleted; }
			set
			{
				if (value == _progressCompleted) return;
				_progressCompleted = value;
				OnPropertyChanged();
			}
		}

		public DateTime EstimatedCompletionTime
		{
			get { return _estimatedCompletionTime; }
			set
			{
				if (value.Equals(_estimatedCompletionTime)) return;
				_estimatedCompletionTime = value;
				OnPropertyChanged();
			}
		}

		public int ProgressBarResolution { get; set; } = 10000;

		public long AverageSpeed
		{
			get { return _averageSpeed; }
			set
			{
				if (value == _averageSpeed) return;
				_averageSpeed = value;
				OnPropertyChanged();
			}
		}

		public long CurrentSpeed
		{
			get { return _currentSpeed; }
			set
			{
				if (value == _currentSpeed) return;
				_currentSpeed = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public WpfAppender Appender
		{
			get { return _appender; }
			set
			{
				if (Equals(value, _appender)) return;

				_appender = value;
				OnPropertyChanged();
			}
		}

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}